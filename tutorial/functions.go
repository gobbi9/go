package main

import (
	"fmt"
	"math"
	"math/cmplx"
)

//custom types
type I64 int64
type C complex128

func multiply(a int32, b int32) int32 {
	return a*b
}

func divide (a,b float32) float32{
	return a/b
}

func area(radius int64) float64{
	return math.Pi*float64(radius*radius)
}

func normalize(x,y float64) (float64,float64){
	norm := math.Sqrt(x*x + y*y)
	return  x/norm, y/norm
}

func sineAndCos(t float64) (a,b float64){
	a = math.Sin(t)
	b = math.Cos(t)
	return
}

func sayHi(){
	fmt.Println("Hi")
}

func main(){
	sayHi()
	fmt.Println("15*20 = ", multiply(15,20))
	fmt.Println("pi/4 = ", divide(math.Pi, 4))
	fmt.Println("Area of circle with radius 10 is", area(10))
	a,b := 3.,4.
	x,y := normalize(a,b)
	const msg = "Norm of vector (%.2f,%.2f) is (%.2f,%.2f)\n"
	fmt.Printf(msg,a,b,x,y)
	a1,b1 := sineAndCos(math.Pi)
	fmt.Printf("(%.2f, %.2f)\n",a1,b1)

	var i,j int
	i = 0; j = 0
	i += j

	var p,q float64 = 0.0, 1.0
	p += q

	xx,yy := true, 1.0/5
	fmt.Println(xx,yy)

	var k1,k2 I64 = 10,20
	i1 := C(1 - cmplx.Sqrt(-1))
	i2 := C(1 + cmplx.Sqrt(-1))

	fmt.Println(k1,k2,i1*i2)
	
	//casting
	var aa int64 = 200;
	var ab int32 = int32(aa)
	fmt.Println(aa,ab)

	//constants
	const E = math.E
	var cPi = complex128(math.Pi)
	fmt.Println(cmplx.Pow(E, cmplx.Sqrt(-1)*cPi))

}

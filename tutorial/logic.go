package main

import (
	"fmt";"math"
)

func sum(a,b int64) (sum int64){
	for i := a; i <= b; i++{
		sum += i
	}
	return
}

//check if 3 points belong to the same line
func checkPoints(x1,y1,x2,y2,x3,y3 float64) (bool){
	return ((y2-y1)/(x2-x1))*(x3-x1) + y1 - y3 == 0
}

// numbers of 4 algarisms that satisfy
// xyzw : (xy + zw)^2 == xyzw
// ex.: (20 + 25)^2 = 2025
func printNumbers(){
	for i := 1000; i<10000; i++{
		partA := i / 100
		partB := i % 100
		//ugly
		if 
		n := int64(math.Pow(float64(partA + partB), 2)); 
		n == int64(i) {
			fmt.Println(i)
		}
	}

}

func factorial(n int64) (int64){
	var r, i int64 = 1,1
	//"while"
	for i<=n{
		r *= i
		i++
	}
	return r
}

/*
infinite loop:
	for {
	}
*/

func fibonacci(n int64) (int64){
	if n <= 1 { 
		return 1 
	} else {
		return fibonacci(n-1) + fibonacci(n-2) 
	}	
}

func main(){
	fmt.Println(sum(10,20))
	fmt.Println(checkPoints(0,0,1,1,2,2))
	fmt.Println(checkPoints(0,0,1,1,2,3))
	printNumbers()
	fmt.Println(factorial(11))
	fmt.Println(fibonacci(11))
}

package main

import ("fmt")

type Point struct{
	X float64
	Y float64
}

func main(){
	var p Point
	p = Point{0.5,1}
	p2 := Point{X:1,Y:.5}
	p2.X = 1.5
	var q *Point
	q = &p
	(*q).X = 2 //is the same as q.X
	var r **Point
	r = &q // or r := &q
	s := q
	fmt.Println(p,*q, **r, s)

	var myPoint *Point
	myPoint = new(Point)
	myPoint = q
	myPoint.X = 5
	fmt.Println(q, p, myPoint)

	aPoint := new(Point)
	aPoint.X, aPoint.Y = 0,1
	fmt.Println(*aPoint)
}

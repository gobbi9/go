package main

import ("fmt";"math")

type Vector struct{
	coords [3]float64
}

func (v Vector) norm()(float64){
	return math.Sqrt(math.Pow(v.coords[0], 2) + 
		math.Pow(v.coords[1],2)+ math.Pow(v.coords[2],2))
}

func (v *Vector) normalize(){
	normV := v.norm()
	for i:=0; i<len(v.coords); i++ {
		v.coords[i] /= normV
	}
}

func simpleRange(n int64)([]int64){
	var i int64
	array := make([]int64, n)
	for i=0; i<n; i++{
		array[i] = i
	}
	return array
}

func printArray(arr []int64){
	for i, v := range arr{
		fmt.Printf("Index: %d Value: %d\n", i, v)
	}
}

func main(){	
	array := simpleRange(10)
	fmt.Println(array)

	printArray(array)

	v := new(Vector)
	v.coords[0], v.coords[1], v.coords[2] = 3.0,4.0,5.0
	fmt.Println(v.norm())
	v.normalize()
	fmt.Println(*v)
}


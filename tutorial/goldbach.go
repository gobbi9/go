package main

import ("fmt";"math")

func isPrime(number int64) (bool){
	var n int64
	n = int64(math.Abs(float64(number)))
	if n <= 1 {
		return false
	}else{
		if n == 2{
			return true
		}else{
			if n % 2 == 0{
				return false
			}else{
				var i,sqrt int64
				sqrt = int64(math.Ceil(math.Sqrt(float64(n))))
				for i= 3; i<=sqrt; i+=2{
					if n % i == 0{
						return false
					}
				}
			}
		}
	}
	return true
}

func goldbach(n int64) (a,b int64){
	a,b = 0,0
	if n % 2 != 0 || n <= 2 {
		return
	}

	var i int64
	for i=2; i<=n; i++{
		if isPrime(i){
			a = i
			b = n - a
			if isPrime(b){
				//fmt.Println(a,b)
				return
			}
		}
	}

	return 0,0
}

func main(){

	var k int64
	for k = 4 ; k<=1000; k+=2{
		x,y := goldbach(k)
		fmt.Println(k, "=", x ,"+", y)
	}
}

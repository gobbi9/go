package main

import "fmt"

type Node struct {
	value int
	next *Node
}

type LinkedList struct {
	head *Node
	size int		
}

func (list *LinkedList) allocate(size int){
	if size <= 0 {
		return
	}
	
	pointer := new (Node)
	list.head = pointer
	list.size = size
	
	for i:=0; i<size; i++{
		pointer.value = i
		if i < size - 1 {
			pointer.next = new (Node)
			pointer = pointer.next
		}
	}
}


func (list *LinkedList) String() string {
	out := "["
	pointer := list.head
	for pointer != nil{
		out += fmt.Sprintf(" %d ", pointer.value)
		pointer = pointer.next
	}
	out += "]"
	return out
}


func main() {
	//const n = 5
	//var array [n]int
	
	//n := 5
	//array := make([]int, n)
	
	/*for i:=0; i<n; i++{
		array[i] = i*i + 1
	}
	
	pointer := new (Node)
	start := pointer
	
	for i, v := range array{
		pointer.value = v
		if i < len(array) - 1 {
			pointer.next = new (Node)
			pointer = pointer.next
		}
	}

	if pointer == start{
		start, pointer = nil, nil
		fmt.Println("Empty")	
	}
	
	for start != nil{
		fmt.Println(start.value)
		start = start.next
	}*/
	
	list := new (LinkedList)
	list.allocate(5)
	fmt.Println(list)
}

